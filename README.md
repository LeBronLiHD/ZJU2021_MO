# ZJU2021_MO

> ZJU2021_MO

- 控制学院机器学习实训课程[MO platform](https://mo.zju.edu.cn/)
- 第一题什么都不用改，`main.py line:139 select_feature_number`记得在训练与提交测试的时候分别为`12`与`15`
- 第二题改动也很少，多提交几次就好
- 第三题我也是照着原模型进行修改，主要是神经网络层数的增加以及神经元个数的增加，学习率、迭代次数、学习率修改函数进行调参即可
- 第四题[修改较多](https://github.com/QikaiXu/Writer-Style-Recognition)，但是最好再引入一下`attention`机制
- MyScore

![1](https://github.com/LeBronLiHD/ZJU2021_MO/blob/main/0-MyScore/1.png)

![1](https://github.com/LeBronLiHD/ZJU2021_MO/blob/main/0-MyScore/2.png)

![1](https://github.com/LeBronLiHD/ZJU2021_MO/blob/main/0-MyScore/3.png)

![1](https://github.com/LeBronLiHD/ZJU2021_MO/blob/main/0-MyScore/4.png)
